(function() {
  'use strict';

  const assign = require('object-assign');
  const jwt = require('jsonwebtoken');
  const crypto = require('crypto');


  const defaults = {
    key: process.env.REDIS_VAULT_KEY || '8y2fvnv9gv845y&b38&h3hd8',
    salt: process.env.REDIS_VAULT_SALT || 'hf74hbd&63gd6%2gd6eg46G',
    secret: process.env.REDIS_VAULT_SECRET,
    expiration: process.env.REDIS_VAULT_EXPIRATION || 86400,
    algorithm: process.env.REDIS_VAULT_ALGORITHM || 'sha256'
  };


  const iv = (init) => {
    return Buffer.from("abcdefghijklmnopqrstuvwxyz", 'utf8').slice(0,16);
    // const options = { weekday: 'long', year: 'numeric', month: 'long', day: 'numeric' };
    // const pseudoRandom = (new Date()).toLocaleDateString('nl-nl', options) + init;
    // const result = Buffer.from(crypto.createHash('sha256').update(pseudoRandom).digest('hex'),'hex').slice(0, 16);
    // return result;
  }

  const encrypt = (text, secret, iv) => {
    const secretBytes = Buffer.from(secret, "hex").slice(0, 32);
    const cipher = crypto.createCipheriv('aes-256-cbc', secretBytes, iv);
    const encrypted = Buffer.concat([cipher.update(text), cipher.final()]);
    return encrypted.toString('hex');
  };

  const decrypt = (encrypted, secret, iv) => {
    const secretBytes = Buffer.from(secret, "hex");
    if (secretBytes.length !== 32) {
      return "";
    }

    const decipher = crypto.createDecipheriv('aes-256-cbc', secretBytes, iv);
    const decrpyted = Buffer.concat([decipher.update(Buffer.from(encrypted, 'hex')), decipher.final()]);
    return decrpyted.toString();
  };

  function wrapper(options) {
    // if options are static (either via defaults or custom options passed in), wrap in a function
    let optionsCallback = null;
    if (typeof options === 'function') {
      optionsCallback = options;
    }
    else {
      optionsCallback = function(req, cb) {
        cb(options);
      };
    }

    return {
      store: function(req, res, content) {

        return new Promise(function(resolve, reject) {
          optionsCallback(req, function(options) {
            const o = assign({}, defaults, options);
            const { cache } = o;
            const userKey = crypto.createHash('sha256').update(typeof content === 'string' ? content : JSON.stringify(content)).digest('hex');
            let vaultKey = crypto.createHash(o.algorithm).update(o.salt + req.tenant+ userKey).digest('hex');
            let vaultSecret = crypto.createHash(o.algorithm).update(o.secret + vaultKey).digest('hex');

            jwt.sign(content, vaultSecret, (err, vaultValue) => {
              if (err) {
                return reject(err)
              }
              
              const encrypted= encrypt(vaultValue, userKey, iv(vaultKey));
              cache.set(vaultKey, encrypted, 'EX', o.expiration, (err) => {
                if (err) {
                  return reject(err)
                }
                resolve(userKey);
              });
            });
          });
        });
      },


      retreive: function(req, res, userKey) {
        return new Promise(function(resolve, reject) {
          return optionsCallback(req, function(options) {
            const o = assign({}, defaults, options);
            const { cache } = o;
            let vaultKey = crypto.createHash(o.algorithm).update(o.salt + req.tenant + userKey).digest('hex');
            cache.get(vaultKey, (err, vaultValue) => {
              if(err) {
                return reject(err)
              }
              if (!vaultValue) {
                return reject('Empty vault')
              }
              const decrypted = decrypt(vaultValue, userKey, iv(vaultKey));
              let vaultSecret = crypto.createHash(o.algorithm).update(o.secret + vaultKey).digest('hex');
              jwt.verify(decrypted, vaultSecret, function(err, content) {
                if (err) {
                  return reject(err);
                }
                resolve(content);
              });
            });
          });
        });
      },

      clear: function(req, res, content) {
        return new Promise(function(resolve, reject) {
          optionsCallback(req, function(options) {
            const o = assign({}, defaults, options);
            const { cache } = o;
            let vaultKey = crypto.createHash(o.algorithm).update(o.salt + req.tenant).digest('hex');
            cache.del(vaultKey, (err) => {
              if (err) {
                reject(err)
              }
              resolve();
            });
          });
        });
      },
    };
  }
  module.exports = wrapper;
}());
